#include "ecbm_core.h"  //统一加载核心头文件
#if (ECBM_EDS_LIB_EN)   //检查本库有没有被使能
/*--------------------------------------程序定义-----------------------------------*/
/*-------------------------------------------------------
eds检查函数。
-------------------------------------------------------*/
u8 eds_check(u16 addr){
    u8 dat_u8,_dat_u8;
    dat_u8=eeprom_read(addr+0);         //读出第一个字节的信息，
    if(dat_u8!='E')return EDS_ERR_NULL; //第一个字节不是E,说明不是EDS。
    dat_u8=eeprom_read(addr+1);         //第一个字节正确的话，读出第二个字节的信息。
    if(dat_u8!='D')return EDS_ERR_NULL; //第二个字节是不是D,不是D说明不是EDS。
    dat_u8=eeprom_read(addr+2);         //第二个字节正确的话，读出第三个字节的信息。
    if(dat_u8!='S')return EDS_ERR_NULL; //第三个字节是不是S,不是S说明不是EDS。
    dat_u8 =eeprom_read(addr+3);        //读出ID，
    _dat_u8=eeprom_read(addr+4);        //还有ID反码。
    if((dat_u8==(~_dat_u8))&&(dat_u8==(u8)(addr/512))){//ID格式也正确的话，这就是一个合格的EDS。
        return EDS_OK;  //返回OK。
    }
    return EDS_ERR_ID;  //走到这里，说明是最后的ID验证不通过，返回ID错误。
}
/*-------------------------------------------------------
eds擦除函数。
-------------------------------------------------------*/
void eds_erase(u8 sector_start,u8 sector_szie){
    u8 i_u8;
    for(i_u8=0;i_u8<sector_szie;i_u8++){        //初始化的时候，就要擦除用到的扇区了。
        eeprom_erase((u16)(sector_start+i_u8)*512);//计算地址，并擦除该地址。
    }
}
/*-------------------------------------------------------
eds初始化函数。
-------------------------------------------------------*/
u8 eds_init(u8 sector_start,u8 sector_szie,u16 pack_len){
    u16 addr_u16;
    if(sector_start>128)return EDS_ERR_SECTOR; //限制扇区起始地址不要超过128，因为地址不超过128*512=65536。
    if(sector_szie >128)return EDS_ERR_SECTOR; //限制数量不要超过128，即总数不超过128*512=65536。
    if(sector_szie ==0 )return EDS_ERR_SECTOR; //扇区数量也不能是0。
    addr_u16=sector_start*512;      //获取开始地址。
    if(eds_check(addr_u16)!=EDS_OK){//检查该地址是否有EDS。
        eds_erase(sector_start,sector_szie);    //没有就擦除。
        eeprom_write(addr_u16+0,'E');           //写入标识符。
        eeprom_write(addr_u16+1,'D');
        eeprom_write(addr_u16+2,'S');
        eeprom_write(addr_u16+3, sector_start); //写入管理ID。
        eeprom_write(addr_u16+4,~sector_start); //写入ID的反码。
        eeprom_write(addr_u16+5,sector_szie);   //写入管理的扇区数量。
        eeprom_write(addr_u16+6,(u8)(pack_len>>8));//写入包的长度。
        eeprom_write(addr_u16+7,(u8)(pack_len   ));
        return EDS_DATA_VOID;                   //重置了之后，此时EDS内无数据。
    }
    return EDS_OK;
}
/*-------------------------------------------------------
eds写函数。
-------------------------------------------------------*/
u8 eds_write(u8 sector_start,u8 * buf){
    u16 start_addr_u16,limit_addr_u16,data_len_u16,count_u16,write_addr_u16;
    u8  read_buf_u8a[8],res_u8,i_u8;
    start_addr_u16=(u16)(sector_start)*512; //计算出开始地址。
    res_u8=eds_check(start_addr_u16);       //检查EDS。
    if(res_u8!=EDS_OK){                     //如果不正常，
        return res_u8;                      //返回错误码。
    }                                       //正常就往下走。
    for(i_u8=0;i_u8<8;i_u8++){              //要读信息区出来。
        read_buf_u8a[i_u8]=eeprom_read(start_addr_u16+(u16)(i_u8));
    }
    limit_addr_u16=start_addr_u16+(u16)(read_buf_u8a[5])*512;//合成限制地址。
    data_len_u16=(u16)(read_buf_u8a[6])*256+(u16)(read_buf_u8a[7]);//合成包长度。
    count_u16=0;//统计值清零。
    for(i_u8=8;i_u8<64;i_u8++){//读标记区的数据。
        res_u8=eeprom_read(start_addr_u16+(u16)(i_u8));//读一个字节。
        //break只能中断一层语句，所以不能用switch。
        if(res_u8==0xFF){count_u16+=0;eeprom_write(start_addr_u16+(u16)(i_u8),0x7F);break;}
        if(res_u8==0x7F){count_u16+=1;eeprom_write(start_addr_u16+(u16)(i_u8),0x3F);break;}
        if(res_u8==0x3F){count_u16+=2;eeprom_write(start_addr_u16+(u16)(i_u8),0x1F);break;}
        if(res_u8==0x1F){count_u16+=3;eeprom_write(start_addr_u16+(u16)(i_u8),0x0F);break;}
        if(res_u8==0x0F){count_u16+=4;eeprom_write(start_addr_u16+(u16)(i_u8),0x07);break;}
        if(res_u8==0x07){count_u16+=5;eeprom_write(start_addr_u16+(u16)(i_u8),0x03);break;}
        if(res_u8==0x03){count_u16+=6;eeprom_write(start_addr_u16+(u16)(i_u8),0x01);break;}
        if(res_u8==0x01){count_u16+=7;eeprom_write(start_addr_u16+(u16)(i_u8),0x00);break;}
        if(res_u8==0x00){count_u16+=8;}
    }
    write_addr_u16=start_addr_u16+64+count_u16*data_len_u16;//拼接出写入地址。
    if((write_addr_u16>=limit_addr_u16)||((limit_addr_u16-write_addr_u16)<data_len_u16)){  
    //如果写入地址超过限制了，或者剩余的空间不够写。
        eds_erase(read_buf_u8a[3],read_buf_u8a[5]); //清除片区。
        for(i_u8=0;i_u8<8;i_u8++){                  //还原信息区。
            eeprom_write(start_addr_u16+i_u8,read_buf_u8a[i_u8]);
        }
        eeprom_write(start_addr_u16+8,0x7F);        //打上第一个数据的标记。
        write_addr_u16=start_addr_u16+64;           //重置写入地址。
    }
    for(count_u16=0;count_u16<data_len_u16;count_u16++){    //写入数据。
        eeprom_write(write_addr_u16+count_u16,buf[count_u16]);
    }
    return EDS_OK;  //写入成功。
}
/*-------------------------------------------------------
eds读函数。
-------------------------------------------------------*/
u8 eds_read(u8 sector_start,u8 * buf){
    u16 start_addr_u16,limit_addr_u16,data_len_u16,count_u16,read_addr;
    u8  read_buf_u8a[8],res_u8,i_u8;
    start_addr_u16=(u16)(sector_start)*512; //计算出开始地址。
    res_u8=eds_check(start_addr_u16);       //检查EDS。
    if(res_u8!=EDS_OK){                     //如果不正常，
        return res_u8;                      //返回错误码。
    }                                       //正常就往下走。
    for(i_u8=0;i_u8<8;i_u8++){              //要读信息区出来。
        read_buf_u8a[i_u8]=eeprom_read(start_addr_u16+(u16)(i_u8));
    }
    limit_addr_u16=start_addr_u16+(u16)(read_buf_u8a[5])*512;//合成限制地址。
    data_len_u16=(u16)(read_buf_u8a[6])*256+(u16)(read_buf_u8a[7]);//合成包长度。
    count_u16=0;//统计值清零。
    for(i_u8=8;i_u8<64;i_u8++){//读标记区的数据。
        res_u8=eeprom_read(start_addr_u16+(u16)(i_u8));//读一个字节。
        //break只能中断一层语句，所以不能用switch。
        if(res_u8==0xFF){count_u16+=0;break;}
        if(res_u8==0x7F){count_u16+=1;break;}
        if(res_u8==0x3F){count_u16+=2;break;}
        if(res_u8==0x1F){count_u16+=3;break;}
        if(res_u8==0x0F){count_u16+=4;break;}
        if(res_u8==0x07){count_u16+=5;break;}
        if(res_u8==0x03){count_u16+=6;break;}
        if(res_u8==0x01){count_u16+=7;break;}
        if(res_u8==0x00){count_u16+=8;}
    }
    if(count_u16==0)return EDS_DATA_VOID;//经过了上面的循环之后，如果统计值还是0，就说明没有数据。
    read_addr=start_addr_u16+64+(count_u16-1)*data_len_u16;//拼接出读取地址。
    for(count_u16=0;count_u16<data_len_u16;count_u16++){//读出数据。
        buf[count_u16]=eeprom_read(read_addr+count_u16);
    }
    return EDS_OK;  //读取成功。
}
#endif  //和最上面的#ifndef配合成一个程序段。
        //以一个空行为结尾。