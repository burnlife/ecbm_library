#ifndef _ECBM_EDS_H_//头文件防止重加载必备，先看看有没有定义过这个，定义说明已经加载过一次了。
#define _ECBM_EDS_H_//没定义说明是首次加载，那么往下执行。并且定义这个宏定义，防止下一次被加载。
/*-------------------------------------------------------------------------------------
The MIT License (MIT)

Copyright (c) 2023 奈特

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

免责说明：
    本软件库以MIT开源协议免费向大众提供。作者只保证原始版本是由作者在维护修BUG，
其他通过网络传播的版本也许被二次修改过，由此出现的BUG与作者无关。而当您使用原始
版本出现BUG时，请联系作者解决。
                            ***************************
                            * 联系方式 ：进群778916610 *
                            * 若1群满人：进群927297508 *
                            ***************************
------------------------------------------------------------------------------------*/
/*-------------------------------------头文件加载-----------------------------------*/
#include "ecbm_core.h"    //ECBM库的头文件，里面已经包含了STC8的头文件。
/*---------------------------------------宏定义------------------------------------*/
#define EDS_OK          (0)   //EDS所有都正常。
#define EDS_ERR_NULL    (1)   //找不到EDS。
#define EDS_ERR_ID      (2)   //EDS中的ID错误。
#define EDS_ERR_SECTOR  (3)   //EDS初始化时扇区没设置对。
#define EDS_DATA_VOID   (4)   //EDS内没有数据。
/*--------------------------------------程序定义-----------------------------------*/
/*-------------------------------------------------------
函数名：eds_check
描  述：eds检查函数。
输  入：
    addr    起始地址。
输  出：无
返回值：
    EDS_OK          EDS所有都正常。
    EDS_ERR_NULL    找不到EDS。
    EDS_ERR_ID      EDS中的ID错误。
创建者：奈特
调用例程：无
创建日期：2022-04-21
修改记录：
-------------------------------------------------------*/
extern u8 eds_check(u16 addr);
/*-------------------------------------------------------
函数名：eds_erase
描  述：eds擦除函数。
输  入：
    sector_start    起始扇区。
    sector_szie     扇区数量/总大小。
输  出：无
返回值：无
创建者：奈特
调用例程：无
创建日期：2022-04-21
修改记录：
-------------------------------------------------------*/
extern void eds_erase(u8 sector_start,u8 sector_szie);
/*-------------------------------------------------------
函数名：eds_init
描  述：eds初始化函数。
输  入：
    sector_start    起始扇区。
    sector_szie     扇区数量/总大小。
    pack_len        数据包大小。
输  出：无
返回值：
    EDS_OK          EDS所有都正常。
    EDS_ERR_SECTOR  EDS初始化时扇区没设置对。
    EDS_DATA_VOID   EDS内没有数据。
创建者：奈特
调用例程：无
创建日期：2022-04-21
修改记录：
-------------------------------------------------------*/
extern u8 eds_init(u8 sector_start,u8 sector_szie,u16 pack_len);
/*-------------------------------------------------------
函数名：eds_write
描  述：eds写函数。
输  入：
    sector_start    起始扇区。
    buf             要写入的数据包。
输  出：无
返回值：
    EDS_OK          EDS所有都正常。
    EDS_ERR_NULL    找不到EDS。
    EDS_ERR_ID      EDS中的ID错误。
    EDS_DATA_VOID   EDS内没有数据。
创建者：奈特
调用例程：无
创建日期：2022-04-21
修改记录：
-------------------------------------------------------*/
extern u8 eds_write(u8 sector_start,u8 * buf);
/*-------------------------------------------------------
函数名：eds_read
描  述：eds读函数。
输  入：
    sector_start    起始扇区。
输  出：
    buf             读取到的数据包。
返回值：
    EDS_OK          EDS所有都正常。
    EDS_ERR_NULL    找不到EDS。
    EDS_ERR_ID      EDS中的ID错误。
    EDS_DATA_VOID   EDS内没有数据。
创建者：奈特
调用例程：无
创建日期：2022-04-21
修改记录：
-------------------------------------------------------*/
extern u8 eds_read(u8 sector_start,u8 * buf);
#endif  //和最上面的#ifndef配合成一个程序段。
        //以一个空行为结尾。